setup_run_control() {
  ## Execute commands needed to be run upon initialization of any environment.
  # Ensure tmux doesn't mess with PATH on MacOS.
  if [ -x /usr/libexec/path_helper ]; then
    PATH=''
    source /etc/profile
  fi

  # Setup locations in PATH variable.
  prepend_to_path ~/.local/bin
  prepend_to_path ~/.nimble/bin
  prepend_to_path ~/.pyenv/bin
  prepend_to_path ~/.pyenv/shims
  prepend_to_path ~/.racket/bin

  # Enable running programs installed through nix.
  if [ -e ~/.nix-profile/etc/profile.d/nix.sh ]; then
    source ~/.nix-profile/etc/profile.d/nix.sh
  fi

  # Make nix programs searchable in menu.
  export XDG_DATA_DIRS=~/.nix-profile/share:~/.share:"''${XDG_DATA_DIRS:-/usr/local/share/:/usr/share/}"

  # Setup pyenv.
  eval "$(pyenv init -)"
}

setup_profile() {
  ## Execute commands needed to be run upon initialization of interactive environment.

  # Setup environment variables.
  export BASH_SILENCE_DEPRECATION_WARNING=1 # Silence MacOS default shell warning message.
  export EDITOR=nvim # Allow tmuxinator to run.

  # Setup prompt.
  export PS1="↳ with $(
    tput setaf 5
    tput bold
  )\$?$(tput sgr0) at $(
    tput setaf 5
    tput bold
  )\t$(tput sgr0)\n\nfrom $(
    tput setaf 4
    tput bold
  )\H$(tput sgr0) as $(
    tput setaf 6
    tput bold
  )\u$(tput sgr0) in $(
    tput setaf 2
    tput bold
  )\`shorten_path\`$(tput sgr0)\`get_branch\`\`get_environment\`\n\[$(tput sgr0)\]"
  export VIRTUAL_ENV_DISABLE_PROMPT=1

  # Setup aliases.
  alias cat=bat
  alias diff=icdiff
  alias find=fd
  alias grep=rga
  alias less=bat
  alias ls=exa
  alias mux=tmuxinator
  alias rm=trash
  alias top=btm
  alias tree="exa --tree"
  alias vim=nvim
}

prepend_to_path() {
  ## Amalgamates paths to the PATH variable avoiding duplicate references.

  if ! echo "${PATH}" | egrep -q "(^|:)${1}($|:)"; then
    export PATH="${1}:${PATH}"
  fi
}

shorten_path() {
  ## Modified from: https://stackoverflow.com/a/19569863/724124

  begin=""
  homebegin=""
  shortbegin=""
  current=""
  end="${2:-$(pwd)}/" # The unmodified rest of the path.
  end="${end#/}"      # Strip the first /
  shortenedpath="${end}"

  shopt -q nullglob && NGV="-s" || NGV="-u"
  shopt -s nullglob

  while [[ "${end}" ]]; do
    current="${end%%/*}" # Everything before the first /
    end="${end#*/}"      # Everything after the first /

    shortcur="${current}"
    for ((i = ${#current} - 2; i >= 0; i--)); do
      [[ ${#current} -le 20 ]] && [[ -z "${end}" ]] && break
      subcurrent="${current:0:i}"
      matching=("${begin}/${subcurrent}"*)         # Array of all files that start with ${subcurrent}
      ((${#matching[*]} != 1)) && break        # Stop shortening if more than one file matches
      [[ -z "${end}" ]] && shortcur="${subcurrent}…" # Add character filler at the end of this string
      [[ -n "${end}" ]] && shortcur="${subcurrent}…" # Add character filler at the end of this string
    done

    begin="${begin}/${current}"
    homebegin="${homebegin}/${current}"
    [[ "${homebegin}" =~ ^"${HOME}"(/|$) ]] && homebegin="~${homebegin#${HOME}}" # Convert HOME to ~
    shortbegin="${shortbegin}/${shortcur}"
    [[ "${homebegin}" == "~" ]] && shortbegin="~" # Use ~ for home
    shortenedpath="${shortbegin}/${end}"
  done

  shortenedpath="${shortenedpath%/}" # Strip trailing /
  shortenedpath="${shortenedpath#/}" # Strip leading /

  [[ ! "${shortenedpath}" =~ ^"~" ]] && printf "/${shortenedpath}" # Make sure it starts with /
  [[ "${shortenedpath}" =~ ^"~" ]] && printf "${shortenedpath}"    # Don't use / for home dir

  shopt "${NGV}" nullglob # Reset nullglob in case this is being used as a function.
}

toggle_hyper_background() {
  # Swap Hyper background plugin.
  if [[ -n "$(hyper list | grep solarized-light)" ]]; then
    hyper uninstall hyper-solarized-light && hyper install hyper-solarized-dark
  elif [[ -n "$(hyper list | grep solarized-dark)" ]]; then
    hyper uninstall hyper-solarized-dark && hyper install hyper-solarized-light
  else
    hyper install hyper-solarized-dark
  fi
}

get_environment() {
  ## Print environmnet name if active.

  if [[ -n "${VIRTUAL_ENV}" ]]; then
    echo " under $(
      tput setaf 1
      tput bold
    )${VIRTUAL_ENV##*/}$(tput sgr0)"
  fi
}

get_branch() {
  ## Print branch name if in git repository.

  if git status &>/dev/null; then
    echo " on $(
      tput setaf 3
      tput bold
    )$(git branch --show-current)$(tput sgr0)"
  fi
}

list_todos() {
  ## Find all `TODO` lines in all files within the current directory.
  rg --color always --line-number --max-columns 100 --max-columns-preview 'TODO(\(\w+\))?:' \
    | sort
}

setup_run_control

# Only setup profile when interactive.
if [[ $- == *i* ]]; then
  setup_profile
fi
