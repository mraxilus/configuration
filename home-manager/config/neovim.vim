lua << EOF

-- Install plugin manager.
vim.cmd [[
  let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
  if empty(glob(data_dir . '/autoload/plug.vim'))
    silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
]]

-- Install plugins using vim-plug.
local Plug = vim.fn['plug#']
vim.call('plug#begin', '~/.config/nvim/plugged')
  -- Provide common plugin dependencies.
  Plug 'junegunn/fzf' -- Navigate files in fuzzy fashion.
  Plug 'kyazdani42/nvim-web-devicons' -- Enable Nerd Font icons.
  Plug 'nvim-lua/plenary.nvim' -- Provide useful functions.

  -- Install self-contained plugins.
  Plug 'akinsho/bufferline.nvim' -- Show buffers as tabs.
  Plug 'editorconfig/editorconfig-vim' -- Adhere to project specific settings.
  Plug 'folke/which-key.nvim' -- Make key mappings discoverable.
  Plug 'github/copilot.vim' -- Provide auto completion.
  Plug 'lambdalisue/readablefold.vim' -- Improve fold readability.
  Plug 'lewis6991/gitsigns.nvim' -- Show line status of git managed files.
  Plug 'lukas-reineke/indent-blankline.nvim' -- Display vertical lines at each indentation level.
  Plug 'lukas-reineke/virt-column.nvim' -- Make line columns unobtrusive.
  Plug 'numToStr/Comment.nvim' -- Simplify code commenting.
  Plug 'nvim-lualine/lualine.nvim' -- Make status line more useful.
  Plug 'nvim-telescope/telescope.nvim' -- Provide multi-purpose search overlay utilities.
  Plug 'p00f/nvim-ts-rainbow' -- Highlight matching parenthesis.
  Plug 'phaazon/hop.nvim' -- Provide better text navigation by making motions global.
  Plug 'windwp/nvim-autopairs' -- Pair surrounds automatically.
  Plug 'preservim/vimux'  -- Run commands in tmux pane.
  Plug 'hoschi/yode-nvim' -- Provide sticky overlay of code selections.
  Plug('ms-jpq/chadtree', { ['branch'] = 'chad', ['do'] = 'python3 -m chadtree deps' }) -- Provide file navigation/management.
  -- TODO: Manage virtual envirements with direnv/nix-shell.

  -- Install COQ (with Tabnine).
  Plug('ms-jpq/coq_nvim', { ['branch'] = 'coq', ['do'] = ':COQdeps' }) -- Provide code completion.
  Plug('ms-jpq/coq.artifacts', { ['branch'] = 'artifacts' }) -- Provide code snippets.
  Plug('ms-jpq/coq.thirdparty', {['branch'] = '3p'}) -- Provide additional code completion.

  -- Install Language Server Protocol (LSP).
  Plug 'neovim/nvim-lspconfig'  -- Provide language server configuration support.
  Plug 'williamboman/nvim-lsp-installer' -- Automatically install language servers.

  -- Install automatically switching solarized theme.
  Plug 'lifepillar/vim-solarized8'

  -- Install treesitter.
  Plug('nvim-treesitter/nvim-treesitter', { ['do'] = ':TSUpdate' }) -- Add language specific highlighting support.
  Plug 'romgrk/nvim-treesitter-context' -- Show outer function definition realtive to cursor.
vim.call('plug#end')


-- Configure COQ.
-- Needs to be defined before require: https://github.com/ms-jpq/coq_nvim/issues/403#issuecomment-1046466771
vim.g.coq_settings = {
  auto_start = 'shut-up',
  clients = {
    tabnine = {
      enabled = true,
    },
  },
}


-- Require and define active plugins.
local autopairs = require'nvim-autopairs'
local bufferline = require'bufferline'
local comment = require'Comment'
local coq = require'coq'
local gitsigns = require'gitsigns'
local hop = require'hop'
local indent_blankline = require'indent_blankline'
local lsp_installer = require'nvim-lsp-installer'
local lualine = require'lualine'
local telescope = require'telescope'
local treesitter = require'nvim-treesitter.configs'
local treesitter_context = require'treesitter-context'
local virt_column = require'virt-column'
local which_key = require'which-key'
local yode = require'yode-nvim'


-- Configure plugin options.
autopairs.setup{}
bufferline.setup{
  options = {
    diagnostics = "nvim_lsp",
    enforce_regular_tabs = true,
    show_buffer_close_icons = false,
    sort_by = 'directory',
    show_close_icon = false,
    offsets = { {
      filetype = "CHADTree",
      text = "File Navigate",
    } },
  }
}
comment.setup{}
gitsigns.setup{
  signs = {
    add = {
      hl = 'GitSignsAdd',
      text = '',
      numhl = 'GitSignsAddNr',
      linehl = 'GitSignsAddLn'
    },
    change = {
      hl = 'GitSignsChange',
      text = '',
      numhl = 'GitSignsChangeNr',
      linehl = 'GitSignsChangeLn'
    },
    delete = {
      hl = 'GitSignsDelete',
      text = '',
      numhl = 'GitSignsDeleteNr',
      linehl = 'GitSignsDeleteLn'
    },
    topdelete = {
      hl = 'GitSignsDelete',
      text = '',
      numhl = 'GitSignsDeleteNr',
      linehl = 'GitSignsDeleteLn'
    },
    changedelete = {
      hl = 'GitSignsChange',
      text = '',
      numhl = 'GitSignsChangeNr',
      linehl = 'GitSignsChangeLn'
    },
  },
  numhl = true,
  current_line_blame = true,

}
hop.setup{}
indent_blankline.setup{
  show_current_context = true,
  show_current_context_start = true,
}
lualine.setup{
  options = {
    theme = "solarized",
    always_divide_middle = false,
    global_status = true,
    component_separators = '',
    section_separators = '',
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {
      'hostname',
      'branch',
      -- TODO: Show activated virtual environment (ideally through nix-shell).
    },
    lualine_c = {
      {
        'filename',
        path = 1, -- Use relative path.
        symbols = {modified = ' ', readonly = ' '},
      },
    },
    lualine_x = {
      {
        'diff',
        symbols = {added = ' ', modified = ' ', removed = ' '},
      },
    },
    lualine_y = {'location'},
    lualine_z = {'progress'},
  },
  extensions = { 'chadtree' },
}
lsp_installer.on_server_ready(function(server)
  server:setup(
    coq.lsp_ensure_capabilities()
  )
end)
telescope.setup{
  defaults = {
    mappings = {
      i = {
        ["<esc>"] = "close",
      },
    },
  },
}
treesitter.setup{
  ensure_installed = "all",
  sync_install = false,
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
  },
  rainbow = {
    enable = true,
    extended_mode = true,
    max_file_lines = nil,
  },

}
treesitter_context.setup{}
virt_column.setup{}
which_key.setup{}
yode.setup{}


-- Set mapleader to space key.
vim.g.mapleader = " "

-- Configure operating system interactions.
vim.opt.clipboard = {"unnamed", "unnamedplus"} -- Share clipboard with operating system.
vim.opt.mouse = "a" -- Allow mouse usage.

-- Configure visuals.
vim.opt.colorcolumn = { 72, 88, 100 } -- Visually assist limiting characters per line.
vim.opt.guifont = "RobotoMono Nerd Font:h16" -- Enable special icons.
vim.opt.number = true -- Show line numbers.
vim.opt.scrolloff = 16 -- Reduce cursor claustrophobia.
vim.opt.termguicolors = true -- Enable better color support.
vim.cmd[[au VimEnter * highlight clear SignColumn]] -- Remove background color from empty gutter.

-- Configure Neovide.
vim.g.neovide_refresh_rate = 140  -- Make navigation smooth as fuck.
vim.g.neovide_cursor_trail_length = 100 -- Ensure cursor movement remains followable.
vim.g.neovide_cursor_animation_length = 0.03 -- Make cursor movement feel snappy.
vim.g.neovide_cursor_vfx_mode = "wireframe" -- Provide cursor movement indicator.

-- Configure theme automatically.
vim.cmd[[
  function! ToggleBackground()
    " Toggle background dependent on time of day.
    colorscheme solarized8
    let time = strftime("%H%M")
    if time >= '0800' && time < '1800'
      set background=light
    else
      set background=dark
    endif
  endfunction

  call ToggleBackground()
  autocmd vimenter * ++nested call ToggleBackground() " Set theme to be Solarized colors.
]]

-- Configure indentation.
vim.opt.expandtab = true -- Convert tabs to spaces.
vim.opt.shiftwidth = 2 -- Set indentation size.
vim.opt.smarttab = true -- Automatically tab to indentation level.
vim.opt.tabstop = 2 -- Set size of tab.

-- Configure code folding.
vim.opt.foldenable = false -- Stop overzealous folding.
vim.opt.foldmethod = "expr" -- Allow treesitter to handle folding.
vim.opt.foldminlines = 1 -- Disable unnecesary folding.
vim.opt.foldnestmax = 3 -- Limit folding to sensible level.
vim.cmd 'set foldexpr=nvim_treesitter#foldexpr()' -- Handle folding with treesitter.

-- Clear highlights/minimap with screen.
vim.api.nvim_set_keymap(
  'n',
  '<silent> <C-l>',
  ':pc<cr>:nohlsearch<cr>:call minimap#vim#ClearColorSearch()<cr><C-l>',
  {}
)

-- Configure Vimux.
vim.g.VimuxHeight = "40" -- Make pane larger.
vim.g.VimuxOrientation = "h" -- Open pane on side instead of bottom.
vim.g.VimuxUseNearest = 1 -- Use available pane if already exists.
vim.g.VimuxRunnerType = "pane" -- Ensure use of pane.

-- Auto format on save.
vim.cmd[[autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting_sync()]]


-- Configure Hop movement.
vim.api.nvim_set_keymap('', 'h', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = true, inclusive_jump = false })<cr>", {})
vim.api.nvim_set_keymap('', 'j', "<cmd>lua require'hop'.hint_lines({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = false, inclusive_jump = false })<cr>", {})
vim.api.nvim_set_keymap('', 'k', "<cmd>lua require'hop'.hint_lines({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = false, inclusive_jump = false })<cr>", {})
vim.api.nvim_set_keymap('', 'l', "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = true, inclusive_jump = false })<cr>", {})

vim.api.nvim_set_keymap('', 'f', "<cmd>lua require'hop'.hint_char1({ direction = nil, current_line_only = false, inclusive_jump = false })<cr>", {})
vim.api.nvim_set_keymap('o', 'f', "<cmd>lua require'hop'.hint_char1({ direction = nil, current_line_only = false, inclusive_jump = true })<cr>", {})
vim.api.nvim_set_keymap('', 'F', "<cmd>lua require'hop'.hint_char1({ multi_windows = true, direction = nil, current_line_only = false, inclusive_jump = false })<cr>", {})

vim.api.nvim_set_keymap('', 't', "<cmd>lua require'hop'.hint_char1({ direction = nil, current_line_only = false, inclusive_jump = false })<cr>", {})
vim.api.nvim_set_keymap('o', 't', "<cmd>lua require'hop'.hint_char1({ direction = nil, current_line_only = false, inclusive_jump = false })<cr>", {})

vim.api.nvim_set_keymap('', 'w', "<cmd>lua require'hop'.hint_words({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = false, inclusive_jump = false })<cr>", {})
vim.api.nvim_set_keymap('o', 'w', "<cmd>lua require'hop'.hint_words({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = false, inclusive_jump = false })<cr>", {})

vim.api.nvim_set_keymap('', 'e', "<cmd>lua require'hop'.hint_words({ hint_position = require'hop.hint'.HintPosition.END, direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = false, inclusive_jump = false })<cr>", {})
vim.api.nvim_set_keymap('o', 'e', "<cmd>lua require'hop'.hint_words({ hint_position = require'hop.hint'.HintPosition.END, direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = false, inclusive_jump = true })<cr>", {})

vim.api.nvim_set_keymap('', 'b', "<cmd>lua require'hop'.hint_words({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = false, inclusive_jump = false })<cr>", {})
vim.api.nvim_set_keymap('o', 'b', "<cmd>lua require'hop'.hint_words({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = false, inclusive_jump = true })<cr>", {})


-- Configure which key.
vim.opt.timeoutlen = 400 -- Show discovery pop-up faster.
which_key.register({
  ['<tab>'] = { '<cmd>Copilot panel<cr>', 'Copilot' },
  b = {
    name = "Buffer",
    c = { '<cmd>BufferLinePickClose<cr>', 'select Close' },
    d = { '<cmd>bdelete<cr>', 'Delete' },
    f = { '<cmd>Telescope buffers<cr>', 'Find' },
    s = { '<cmd>BufferLinePick<cr>', 'Select' },
    p = { '<cmd>BufferLineTogglePin<cr>', 'Pin' },
    w = { '<cmd>write<cr>', 'Write' },
  },
  c = {
    name = "Call",
    b = {'<cmd>call ToggleBackground()<cr>', 'toggle Background'},
  },
  f = {
    name = "File",
    f = { '<cmd>Telescope find_files<cr>', 'Find' },
    n = { '<cmd>CHADopen<cr>', 'Navigate' },
    s = { '<cmd>Telescope live_grep<cr>', 'Search' },
  },
  g = {
    name = "Git",
    b = { '<cmd>Gitsigns blame_line<cr>', 'Blame' },
    d = { '<cmd>Telescope git_status<cr>', 'Diff' },
    n = { '<cmd>Gitsigns next_hunk<cr>', 'Next hunk' },
    p = { '<cmd>Gitsigns prev_hunk<cr>', 'Previous hunk' },
    u = { '<cmd>Gitsigns undo_stage_hunk<cr>', 'Undo stage hunk' },
    r = { '<cmd>Gitsigns reset_hunk<cr>', 'Reset hunk' },
    s = { '<cmd>Gitsigns stage_hunk<cr>', 'Stage hunk' },
    v = { '<cmd>Gitsigns preview_hunk<cr>', 'View hunk' },
  },
  h = {
    name = "Help",
    c = { '<cmd>Telescope commands<cr>', 'Commands' },
    k = { '<cmd>Telescope keymaps<cr>', 'Keymaps' },
    t = { '<cmd>Telescope help_tags<cr>', 'Tags' },
    p = { '<cmd>Telescope builtin<cr>', 'Builtins' },
    q = { '<cmd>Telescope quickfix<cr>', 'Quickfix' },
  },
  l = {
    name = "LSP",
    a = { '<cmd>lua vim.buf.lsp.code_action<cr>', 'Actions' },
    d = { '<cmd>Telescope diagnostics<cr>', 'Diagnostics' },
    i = { '<cmd>Telescope lsp_implementations<cr>', 'Implementations' },
    l = { '<cmd>Telescope lsp_definitions<cr>', 'Locate definitions' },
    p = { '<cmd>Telescope builtin<cr>', 'Pickers' },
    r = { '<cmd>Telescope lsp_references<cr>', 'References' },
    s = { '<cmd>Telescope lsp_document_symbols<cr>', 'Symbols' },
    t = { '<cmd>Telescope lsp_type_definitions<cr>', 'Types' },
  },
  t = {
    name = "Tmux",
    c = { '<cmd>VimuxCloseRunner<cr>', 'Close' },
    i = { '<cmd>VimuxInterruptRunner<cr>', 'Interrupt' },
    r = { '<cmd>VimuxPromptCommand<cr>', 'Run' },
    a = { '<cmd>VimuxRunLastCommand<cr>', 'run Again' },
    z = { '<cmd>VimuxZoomRunner<cr>', 'Zoom' },
  },
  w = {
    name = "Window",
    ['<down>'] = { '<cmd>wincmd j<cr>', 'Down' },
    ['<left>'] = { '<cmd>wincmd h<cr>', 'Right' },
    ['<right>'] = { '<cmd>wincmd l<cr>', 'Right' },
    ['<up>'] = { '<cmd>wincmd k<cr>', 'Up' },
    c = { '<cmd>wincmd c<cr>', 'Close' },
    p = { '<cmd>wincmd p<cr>', 'Previous' },
    s = { '<cmd>wincmd W<cr>', 'Switch' },
  },
  y = {
    name = "Yode",
    ['<down>'] = { ':YodeLayoutShiftWinDown<cr>', 'Down' },
    ['<up>'] = { ':YodeLayoutShiftWinUp<cr>', 'Up' },
    p = { ':YodeCreateSeditorFloating<cr><cmd>wincmd w<cr>', 'Pin', mode='v'}, -- Note that `:` is necessary.
    s = { '<cmd>wincmd w<cr>', 'Switch'},
  },
}, {prefix = '<leader>'})

EOF
