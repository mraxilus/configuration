"use strict";
module.exports = {
    config: {
        updateChannel: 'stable',
        fontSize: 16,
        fontFamily: '"RobotoMono Nerd Font", monospace',
        cursorShape: 'BEAM',
        cursorBlink: true,
    },
    plugins: [
      "hyper-solarized-dark",
    ],
};
