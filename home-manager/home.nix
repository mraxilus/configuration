{ config, lib, pkgs, ... }:

{
  # Support non-NixOS operating systems.
  targets.genericLinux.enable =
    if pkgs.stdenv.hostPlatform.isLinux then true else false;

  # Enable and dynamically configure programs.
  programs = {
    home-manager.enable = true; # Prepare home-manager.
    git.enable = true;
  };

  # Copy additional static configuration to system.
  xdg.configFile = {
    "../.bashrc".text = lib.mkAfter (builtins.readFile ./config/bashrc.sh);
    "../.hyper.js".text = lib.mkAfter (builtins.readFile ./config/hyper.js);
    "../.profile".text = lib.mkAfter (builtins.readFile ./config/bashrc);
    "nix/nix.conf".text = lib.mkAfter (builtins.readFile ./config/nix.conf);
    "git/config".text = lib.mkAfter (builtins.readFile ./config/git.toml);
    "bottom/bottom.toml".text = lib.mkAfter (builtins.readFile ./config/bottom.toml);
    "nvim/init.vim".text = lib.mkAfter (builtins.readFile ./config/neovim.vim);
    "tmux/tmux.conf".text = lib.mkAfter (builtins.readFile ./config/tmux.conf);
    "tmuxinator/axilus.yml".text = lib.mkAfter (builtins.readFile ./config/tmuxinator/axilus.yml);
    "tmuxinator/envelop.yml".text = lib.mkAfter (builtins.readFile ./config/tmuxinator/envelop.yml);
    "user-dirs.dirs".text = lib.mkAfter (builtins.readFile ./config/user-dirs.dirs);
  };

  # Configure package manager.
  nixpkgs.config = {
    allowUnfree = true; # Allow proprietary packages.
    allowUnsupportedSystem = # Support MacOS.
      if pkgs.stdenv.hostPlatform.isDarwin then true else false;
  };

  # Install packages.
  home.packages = with pkgs; [
    awscli # Manage Amazon Web Services utilities.
    bat # Alias cat/less for improved file examination.
    bottom # Monitor cpu/memory/network/disk usage.
    cargo # Provide rust package management.
    dos2unix # Fix Windows line endings.
    exa # Alias ls/tree for improved file listings.
    exercism # Download/submit solutions to exercism.io problems.
    fd # Alias find for improved file searching.
    ffmpeg # Record/convert video and audio.
    fzf # Filter/search for files in fuzzy fashion.
    ghq # Manage repositories.
    gibo # Automatically generate .gitignore files.
    google-cloud-sdk # Manage GCP utilities.
    hyperfine # Benchmark terminal commands.
    icdiff # Alias diff for colored comparisons.
    mmv # Rename many files.
    neovim # Alias vim for better text editing.
    nmap # Query local network.
    nodePackages.prettier # Format assortment of code files.
    nodejs # Provide ECMA package management.
    peco # Filter interactively selected lines from stdout with pipes.
    pre-commit # Perform automatic git standards adherence checks.
    ripgrep # Provide search support in Neovim.
    ripgrep-all # Alias grep for usage simplification.
    terminal-colors # Test terminal color support.
    tldr # Simplify man pages.
    tmux # Provide terminal multiplexing and session management.
    tmuxinator # Provide declarative session management for tmux.
    trash-cli # Alias rm for accidental deletion recovery.
    tree-sitter # Provide incremental syntax highlighting.
    wget  # Retrieve online files.
  ];

  # TODO: Update state version on next clean install.
  home.stateVersion = "20.03";
}
