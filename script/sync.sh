#!/usr/bin/env bash

# Debug script.
set -o nounset
set -o errexit

# Setup needed path variables.
if [[ $(uname -a) =~ Darwin ]]; then readlink() { greadlink "$@"; }; fi # Replace MacOS readlink with POSIX version.
DOTFILES_ROOT="$(readlink -f "${BASH_SOURCE[0]}" | xargs dirname)/.."
CURRENT_ROOT="$PWD"
LOG_FILE="$DOTFILES_ROOT/script.log"
cd "$DOTFILES_ROOT"

# Import swiss library.
source "$DOTFILES_ROOT/library/swiss.sh/swiss.sh"

swiss::log::info "Overwrite local device's home manager configuration."
rsync --recursive "$DOTFILES_ROOT"/home-manager/* "$HOME/.config/home-manager/" &>>"$LOG_FILE"

swiss::log::info "Generate dotfiles and update home manager generation."
source "$HOME/.nix-profile/etc/profile.d/nix.sh"
home-manager switch -b backup &>>"$LOG_FILE"
