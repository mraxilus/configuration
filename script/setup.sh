#!/usr/bin/env bash

# Debug script.
set -o nounset
set -o errexit

if [[ $(uname -a) =~ Darwin ]] && ! command -v brew &>/dev/null; then
  echo "Bootstraping MacOS with required libraries."
  xcode-select --install
  curl --silent --location --fail https://raw.githubusercontent.com/Homebrew/install/master/install.sh | sh
  brew install coreutils
fi

# Setup needed path variables.
if [[ $(uname -a) =~ Darwin ]]; then readlink() { greadlink "$@"; }; fi # Replace MacOS readlink with POSIX version.
DOTFILES_ROOT="$(readlink -f "${BASH_SOURCE[0]}" | xargs dirname)/.."
CURRENT_ROOT="$PWD"
LOG_FILE="$DOTFILES_ROOT/script.log"

# Setup additional configuration.
UPDATE_COMMAND_DIRECTORY="$HOME/.local/bin"
UPDATE_COMMAND_NAME="update"

# Import swiss library.
git submodule update --init --remote --merge &>>"$LOG_FILE"
source "$DOTFILES_ROOT/library/swiss.sh/swiss.sh"

if command -v brew &>/dev/null; then
  swiss::log::info "Installing homebrew packages."
  # TODO: Refactor all instances of reading file contents into array into function.
  readarray brew_packages <<<"$(cat "$DOTFILES_ROOT/package/brew.list" | sed "s:#.*$::g" | grep -v "^\s*$")"
  brew install $(printf "%s " "${brew_packages[@]}") &>>"$LOG_FILE"
fi

if swiss::is_windows && ! command -v scoop &>/dev/null; then
  swiss::log::info "Installing scoop."
  powershell.exe -Command "Set-ExecutionPolicy RemoteSigned -scope CurrentUser" &>>"$LOG_FILE"
  powershell.exe -Command "Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')" &>>"$LOG_FILE"
  exec bash                        # Reload shell to access scoop command.
  scoop install git &>>"$LOG_FILE" # Install bucket prerequisites.
  scoop bucket add extras &>>"$LOG_FILE"
fi

if swiss::is_windows; then
  swiss::log::info "Installing scoop packages."
  readarray scoop_packages <<< "$(cat "$DOTFILES_ROOT/package/scoop.list" | sed "s:#.*$::g" | grep -v "^\s*$")"
  scoop install $(printf "%s " "${scoop_packages[@]}") &>>"$LOG_FILE"

  swiss::log::info "Installing winget packages."
  powershell.exe -Command winget import "$(wslpath -m "$DOTFILES_ROOT/package/winget.json")" &>>"$LOG_FILE"
fi

if ! swiss::is_mac && command -v apt-get &>/dev/null; then

  swiss::log::info "Install common apt packages."
  readarray apt_packages <<< "$(cat "$DOTFILES_ROOT/package/apt/common.list" | sed "s:#.*$::g" | grep -v "^\s*$")"
  sudo apt-get install --assume-yes $(printf "%s " "${apt_packages[@]}") &>>"$LOG_FILE"

  if ! swiss::is_windows; then
    swiss::log::info "Install apt repositories for Linux."
    readarray apt_repositories <<< "$(cat "$DOTFILES_ROOT/package/apt/linux/repository.list" | sed "s:#.*$::g" | grep -v "^\s*$")"
    for r in "${apt_repositories[@]}"; do
      sudo apt-add-repository --yes $r &>>"$LOG_FILE"
    done

    swiss::log::info "Install apt repository for Docker."
    sudo mkdir -p /etc/apt/keyrings
    [[ -f /etc/apt/keyrings/docker.gpg ]] && sudo rm -f /etc/apt/keyrings/docker.gpg
    curl --silent --location --fail https://download.docker.com/linux/ubuntu/gpg \
      | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg &>> "$LOG_FILE"
    echo "deb [arch=$(dpkg --print-architecture) \
          signed-by=/etc/apt/keyrings/docker.gpg] \
          https://download.docker.com/linux/ubuntu \
          $(lsb_release --codename --short) stable" \
      | sudo tee /etc/apt/sources.list.d/docker.list &>> "$LOG_FILE"

    swiss::log::info "Install apt repository for GitLab Runner."
    curl --silent --location --fail "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" \
      | sudo os=ubuntu dist=$(lsb_release --codename --short) bash &>> "$LOG_FILE"
    swiss::log::warn "Register local GitLab Runner manually."


    swiss::log::info "Install Linux apt packages."
    readarray apt_packages <<<"$(cat "$DOTFILES_ROOT/package/apt/linux/common.list" | sed "s:#.*$::g" | grep -v "^\s*$")"
    sudo apt-get install --assume-yes $(printf "%s " "${apt_packages[@]}") &>>"$LOG_FILE"
    
    if [[ $(which nvidia-smi) ]]; then
      swiss::log::info "Install NVIDIA packages."
      readarray apt_packages <<<"$(cat "$DOTFILES_ROOT/package/apt/linux/nvidia.list" | sed "s:#.*$::g" | grep -v "^\s*$")"
      sudo apt-get install --assume-yes $(printf "%s " "${apt_packages[@]}") &>>"$LOG_FILE"
    fi

    swiss::log::info "Setup Docker group for user and GitLab Runner."
    [[ ! $(getent group docker) ]] && sudo groupadd docker
    sudo usermod --append --groups docker $USER
    sudo usermod --append --groups docker gitlab-runner
    #newgrp docker
  fi
fi

if command -v flatpak &>/dev/null; then
  swiss::log::info "Installing Flatpak packages."
  readarray flatpak_packages <<<"$(cat "$DOTFILES_ROOT/package/flatpak.list" | sed "s:#.*$::g" | grep -v "^\s*$")"
  flatpak install --assumeyes $(printf "%s " "${flatpak_packages[@]}") &>>"$LOG_FILE"
fi

if ! command -v nix &>/dev/null; then
  swiss::log::info "Installing nix package manager."
  curl --silent --location --fail https://nixos.org/nix/install | sh &>>"$LOG_FILE"
  source ~/.nix-profile/etc/profile.d/nix.sh # Make commands available.
fi

if ! command -v home-manager &>/dev/null; then
  swiss::log::info "Installing home-manager."
  nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager &>>"$LOG_FILE"
  nix-channel --update &>>"$LOG_FILE"
  export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels${NIX_PATH:+:$NIX_PATH}
  nix-shell '<home-manager>' -A install &>>"$LOG_FILE"
fi

if swiss::is_linux && ! swiss::is_windows; then
  swiss::log::info "Configuring egpu-switcher."
  # TODO: Configure egpu-switcher automatically.
  swiss::log::warn 'Authorize eGPU manually, then run `sudo egpu-switcher setup`.'
fi

if ! command -v choosenim &>/dev/null; then
  swiss::log::info "Installing choosenim."
  # TODO: Install choosenim non-interactively.
  swiss::log::warn 'Install choosenim manually with `curl  -L https://nim-lang.org/choosenim/init.sh -sSf | sh`.'
fi

if ! command -v pyenv &>/dev/null; then
  swiss::log::info "Installing pyenv."
  # TODO: Make `remove if exists` function.
  [[ -d "$HOME/.pyenv" ]] && rm -rf "$HOME/.pyenv" 
  curl --silent --location --fail https://pyenv.run | bash &>>"$LOG_FILE"
fi

if ! command -v poetry &>/dev/null; then
  swiss::log::info "Installing poetry."
  curl --silent --location --fail https://install.python-poetry.org | python3 - &>>"$LOG_FILE"
fi

if ! command -v akr &>/dev/null; then
  swiss::log::info "Installing Krypton."
  curl --silent --location --fail https://akamai.github.io/akr-pkg/debian/KEY.gpg | sudo apt-key add - &>>"$LOG_FILE"
  sudo curl --silent --location --fail --output /etc/apt/sources.list.d/akr.list https://akamai.github.io/akr-pkg/debian/akr.list &>>"$LOG_FILE"
  sudo apt-get update &>>"$LOG_FILE"
  sudo apt-get install akr &>>"$LOG_FILE"

  swiss::log::warn 'Pair Krypton to phone manually.'
  touch ~/.ssh/config &>>"$LOG_FILE"  # Avoid `akr setup` bug.
  akr setup &>>"$LOG_FILE"
  akr pair
fi

if ! test -f "$UPDATE_COMMAND_DIRECTORY/$UPDATE_COMMAND_NAME"; then
  swiss::log::info "Creating link to update.sh script as $UPDATE_COMMAND_DIRECTORY/$UPDATE_COMMAND_NAME."
  mkdir -p "$UPDATE_COMMAND_DIRECTORY"
  ln -s "$DOTFILES_ROOT"/script/update.sh "$UPDATE_COMMAND_DIRECTORY/$UPDATE_COMMAND_NAME"
fi

swiss::log::info "Install tmux plugin manager."
[[ -d "$HOME/.tmux/plugins/tpm" ]] && rm -rf "$HOME/.tmux/plugins/tpm"
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm &>>"$LOG_FILE"

if swiss::is_linux && ! swiss::is_windows; then
  # TODO: Install GNOME packages automatically.
  swiss::log::warn "Install GNOME packages manually."
fi

# TODO: Install Zotero packages automatically.
swiss::log::warn "Install Zotero plugins manually."

# TODO: Rename user directories automatically.
swiss::log::warn "Rename user directories to match \`~/.config/user-dirs.dirs\`."

