#!/usr/bin/env bash

# Debug script.
set -o nounset
set -o errexit

# Setup needed path variables.
if [[ $(uname -a) =~ Darwin ]]; then readlink() { greadlink "$@"; }; fi # Replace MacOS readlink with POSIX version.
DOTFILES_ROOT="$(readlink -f "${BASH_SOURCE[0]}" | xargs dirname)/.."
CURRENT_ROOT="$PWD"
LOG_FILE="$DOTFILES_ROOT/script.log"
cd "$DOTFILES_ROOT"

# Import swiss library.
source "$DOTFILES_ROOT/library/swiss.sh/swiss.sh"

if swiss::is_windows; then
  swiss::log::info "Updating winget packages."
  powershell.exe -Command winget upgrade --all &>>"$LOG_FILE"

  swiss::log::info "Updating scoop packages."
  powershell.exe -Command scoop update "*" &>>"$LOG_FILE"
fi

if ! swiss::is_mac && command -v apt-get &>/dev/null; then
  swiss::log::info "Updating apt packages."
  sudo apt-get update --assume-yes &>>"$LOG_FILE"
  sudo apt-get dist-upgrade --assume-yes &>>"$LOG_FILE"
fi

if command -v flatpak &>/dev/null; then
  swiss::log::info "Updating flatpak packages."
  flatpak update --assumeyes &>>"$LOG_FILE"
fi

if command -v brew &>/dev/null; then
  swiss::log::info "Updating brew packages."
  brew update &>>"$LOG_FILE"
  brew upgrade &>>"$LOG_FILE"
  brew upgrade --cask &>>"$LOG_FILE"
fi

# Allow nix to be updated on MacOS.
if swiss::is_mac; then
  export NIXPKGS_ALLOW_UNSUPPORTED_SYSTEM=1
fi

swiss::log::info "Updating nix packages."
nix-channel --update &>>"$LOG_FILE"
nix-env --upgrade &>>"$LOG_FILE"
home-manager switch -b backup &>>"$LOG_FILE"
