#pragma once

// Use default reset settings for Polyglot.
#define RP2040_BOOTLOADER_DOUBLE_TAP_RESET
#define RP2040_BOOTLOADER_DOUBLE_TAP_RESET_TIMEOUT 500U

// Allow MIDI layer to use advanced commands.
// TODO: Verify why this doesn't work.
#define MIDI_ADVANCED