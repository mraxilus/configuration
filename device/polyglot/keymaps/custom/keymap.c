#include QMK_KEYBOARD_H

// Define name aliases for each layer.
enum layers {
    _STENO,
    _QWERTY,
    _GAME,
    _MIDI,
    _MOD1,
    _MOD2,
};

// Define name aliases for custom layer keycodes.
enum keycodes {
    STENO = SAFE_RANGE,  // Ensure keycodes for layers are unique.
    QWERTY,
    GAME,
    MIDI,
    MOD1,
    MOD2,
};

/*
  Setup layer transitions as folows:
    - Default layer is STENO.
    - From STENO you can transition to any other base layer.
    - Each base layer can have modifier keys on bottom row in 1st, 4th, and 5th positions.
    - Bottom row 6th position should return to STENO layer if possible.
*/
#define STENO TO(_STENO)
#define QWERTY TO(_QWERTY)
#define GAME TO(_GAME)
#define MIDI TO(_MIDI)
#define MOD1 MO(_MOD1)
#define MOD2 MO(_MOD2)

// Overload specific keys for modifier comfort.
//   It's best to be conservative; 
//     ideally one key should have one non-mod function.
#define QUO_SFT MT(MOD_RSFT, KC_QUOT) // Make shift symmetrical.
#define ENT_CTL MT(MOD_RCTL, KC_ENT)  // Make control symmetrical.
#define INS_CTL MT(MOD_RCTL, KC_INS)  // Make control symmetrical.
#define TAB_ALT MT(MOD_LALT, KC_TAB)  // Provide alt key.
#define CTL_SFT KC_LCTL | KC_LSFT  // Allow for non-vim selection.

// Define layouts for each layer.
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
   
   // Define stenography layer.
   [_STENO] = LAYOUT_split_3x6_3(
     //|--------|--------|--------|--------|--------|--------|        |--------|--------|--------|--------|--------|--------|
        QWERTY,  STN_S1,  STN_TL,  STN_PL,  STN_HL,  STN_ST1,          STN_ST3, STN_FR,  STN_PR,  STN_LR,  STN_TR,  STN_DR,
        GAME,    STN_S2,  STN_KL,  STN_WL,  STN_RL,  STN_ST2,          STN_ST4, STN_RR,  STN_BR,  STN_GR,  STN_SR,  STN_ZR,
        MIDI,    KC_LSFT, KC_LCTL, KC_LALT, KC_LCMD, KC_SPC,           KC_VOLD, KC_LEFT, KC_DOWN, KC_UP ,KC_RIGHT,  KC_VOLU,
                                   STN_N1,  STN_A,   STN_O,            STN_E,   STN_U,   STN_N2
   ),

  // Define qwerty layer.
  [_QWERTY] = LAYOUT_split_3x6_3(
     //|--------|--------|--------|--------|--------|--------|        |--------|--------|--------|--------|--------|--------|
        KC_ESC,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,             KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_BSPC,
        KC_LSFT, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,             KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, QUO_SFT,
        KC_LCTL, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,             KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, ENT_CTL,
                                   KC_LCMD, TAB_ALT, KC_SPC,           MOD1,    MOD2,    STENO
  ),

  // Define gaming layer.
  [_GAME] = LAYOUT_split_3x6_3(
     //|--------|--------|--------|--------|--------|--------|        |--------|--------|--------|--------|--------|--------|
        KC_T,    KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,             KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_BSPC,
        KC_G,    KC_LSFT, KC_A,    KC_S,    KC_D,    KC_F,             KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, QUO_SFT,
        KC_B,    KC_LCTL, KC_Z,    KC_X,    KC_C,    KC_V,             KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, ENT_CTL,
                                   KC_ESC,  TAB_ALT, KC_SPC,           MOD1,    MOD2,    STENO
  ),

  // Define music layer.
  [_MIDI] = LAYOUT_split_3x6_3(
     //|--------|--------|--------|--------|--------|--------|        |--------|--------|--------|--------|--------|--------|
        MI_C4,   MI_Cs4,  MI_D4,   MI_Ds4,  MI_E4,   MI_F4,            MI_Fs4,  MI_G4,   MI_Gs4,  MI_A4,   MI_As4,  MI_B4,
        MI_C3,   MI_Cs3,  MI_D3,   MI_Ds3,  MI_E3,   MI_F3,            MI_Fs3,  MI_G3,   MI_Gs3,  MI_A3,   MI_As3,  MI_B3,
        MI_C2,   MI_Cs2,  MI_D2,   MI_Ds2,  MI_E2,   MI_F2,            MI_Fs2,  MI_G2,   MI_Gs2,  MI_A2,   MI_As2,  MI_B2,
                                   MI_OC0,  MI_OCTD, MI_OCTU,          MI_TRSD, MI_TRSU, STENO
  ),

  // Define modifier layers for access to additional/special keys.
  [_MOD1] = LAYOUT_split_3x6_3(
     //|--------|--------|--------|--------|--------|--------|        |--------|--------|--------|--------|--------|--------|
        KC_PSCR, KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,            KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_BSPC,
        KC_GRV,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,             KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_BSLS,
        KC_LCTL, KC_F11,  KC_F12,  KC_F13,  KC_F14,  KC_F15,           KC_F16,  KC_F17,  KC_F18,  KC_F19,  KC_F20,  ENT_CTL,
                                   _______, _______, _______,          XXXXXXX, XXXXXXX, XXXXXXX
  ),
  [_MOD2] = LAYOUT_split_3x6_3(
     //|--------|--------|--------|--------|--------|--------|        |--------|--------|--------|--------|--------|--------|
        KC_SCRL, KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT, KC_VOLU,          KC_LCTL, KC_UNDS, KC_PLUS, KC_LCBR, KC_RCBR, KC_DEL,
        KC_TILD, KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC,          KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC_PIPE,
        KC_LCTL, KC_HOME, KC_PGDN, KC_PGUP, KC_END,  KC_VOLD,          CTL_SFT, KC_MINS, KC_EQL,  KC_LBRC, KC_RBRC, INS_CTL,
                                   _______, _______, _______,          XXXXXXX, XXXXXXX, XXXXXXX
  ),
};
