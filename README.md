# [dotfiles][website]

📜 _Configuring run control scripts and applications._

I created this project to uncouple my favorite computer's configuration from my favorite computer.
Imagine if you put all your dotfiles in one version controlled folder,
  and then symlinked them back to their original locations.
That's essentially what this does, with some added bells and whistles.

I heavily tailored the scripts to my specific needs.
If you run these scripts they may make your system unusable.
Use at your own risk.

## 🪛 Install

For installation you must first clone the project.
After which, simply run the setup script:

```sh
./script/setup.sh
```
The script will install a collection of dependencies,
  applications,
  and development tools.
The setup script is designed to work on Linux (`Pop!_OS`), 
  macOS (`Montery`), 
  and Windows (`11` via `WSL`).

## 🧰 Usage

If you want to sync changes from the repository to the resulting locations, run:

```sh
./script/sync.sh
```

To update your system, including platform specific package managers, run:

```sh
./script/update.sh
```

## 🤝 Contributing

Since I mostly use this for my personal systems, I'm unlikely to accept pull requests.
That being said, I would be greatful if you notified me of any security concerns,
  or implementation suggestions.


## ⚖️  License

I licensed this project under the [creative common][license].
You waive this license for your project by contacting me directly.

[website]: https://emmanuel.website
[license]: https://creativecommons.org/licenses/by-nc-sa/4.0
